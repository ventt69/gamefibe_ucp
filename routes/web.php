<?php

Route::get('/', ['uses' => 'PageController@index', 'as' => 'index']);
Route::get('chat', ['uses' => 'PageController@chat', 'as' => 'chat']);

Route::prefix('auth')->group(function () {
    Route::get('login', ['uses' => 'AuthController@getLogin', 'as' => 'auth.login']);
    Route::post('login', ['uses' => 'AuthController@postLogin', 'as' => 'auth.login']);

    Route::get('register', ['uses' => 'AuthController@getRegister', 'as' => 'auth.register']);
    Route::post('register', ['uses' => 'AuthController@postRegister', 'as' => 'auth.register']);

    Route::get('logout', ['uses' => 'AuthController@getLogout', 'as' => 'auth.logout']);
});

Route::prefix('users')->group(function () {
    Route::get('/', ['uses' => 'UserController@all', 'as' => 'user.all']);
    Route::get('{id}', ['uses' => 'UserController@show', 'as' => 'user.show']);
});

Route::prefix('news')->group(function () {
    Route::get('/', ['uses' => 'NewsController@all', 'as' => 'news.all']);
    Route::get('create', ['uses' => 'NewsController@create', 'as' => 'news.create']);
    Route::post('create', ['uses' => 'NewsController@postCreate', 'as' => 'news.create']);
    Route::get('{id}/edit', ['uses' => 'NewsController@edit', 'as' => 'news.edit']);
    Route::post('{id}/edit', ['uses' => 'NewsController@postEdit', 'as' => 'news.edit']);
    Route::get('{id}/delete', ['uses' => 'NewsController@delete', 'as' => 'news.delete']);
    Route::get('{id}/{slug}', ['uses' => 'NewsController@show', 'as' => 'news.show']);
});

Route::prefix('bugs')->group(function () {
    Route::get('/', ['uses' => 'BugController@all', 'as' => 'bug.all']);
    Route::get('create', ['uses' => 'BugController@create', 'as' => 'bug.create']);
    Route::post('create', ['uses' => 'BugController@postCreate', 'as' => 'bug.create']);
    Route::get('{id}', ['uses' => 'BugController@show', 'as' => 'bug.show']);
    Route::get('{id}/edit', ['uses' => 'BugController@edit', 'as' => 'bug.edit']);
    Route::post('{id}/edit', ['uses' => 'BugController@postEdit', 'as' => 'bug.edit']);
    Route::get('{id}/delete', ['uses' => 'BugController@delete', 'as' => 'bug.delete']);
});

Route::prefix('message')->group(function () {
    Route::get('/', ['uses' => 'MessageController@all', 'as' => 'message.all']);
    Route::get('create', ['uses' => 'MessageController@create', 'as' => 'message.create']);
    Route::post('create', ['uses' => 'MessageController@postCreate', 'as' => 'message.create']);
    Route::get('{id}', ['uses' => 'MessageController@show', 'as' => 'message.show']);
    Route::get('{id}/edit', ['uses' => 'MessageController@edit', 'as' => 'message.edit']);
    Route::post('{id}/edit', ['uses' => 'MessageController@postEdit', 'as' => 'message.edit']);
    Route::get('{id}/delete', ['uses' => 'MessageController@delete', 'as' => 'message.delete']);
});

Route::prefix('api/v1')->group(function () {
    Route::get('user_search', ['uses' => 'Api\SearchController@getUsers']);
    Route::get('bug_search', ['uses' => 'Api\SearchController@getBugs']);

    Route::post('chat/fetch', 'Api\ChatController@fetch');
    Route::post('chat/send', 'Api\ChatController@send');
});
