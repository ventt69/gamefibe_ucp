
    var scollBox = $('.shoutbox');
    //var height = $("ul .list-group")[0].scrollHeight;
    var height = 0;

    $("ul li").each(function() {
        height += $(this).outerHeight(true); // to include margins
    });

    $('.chat-messages .list-group').animate({
        scrollTop: height
    });

    window.setInterval(function() {
        $.ajax({
            url: "/api/v1/chat/fetch",
            type: 'post',
            data: { 'fetch': 1 },
            dataType: 'json',

            success: function(data) {
                $('.chat-messages .list-group').html(data.data);
                var text = document.getElementById('.chat-messages .list-group').html;
                var out = emoji.replace_colons(text);
            }
        });
    }, 3000);

    $("#chat-message").keypress(function(evt) {
        if (evt.which == 13) {
            var message = $('#chat-message').val();

            $.ajax({
                url: "/api/v1/chat/send",
                type: 'post',
                data: { 'message': message },
                dataType: 'json',

                success: function(data) {
                    $(data.data).hide().appendTo('.chat-messages .list-group').fadeIn();
                    $('#chat-error').addClass('hidden');
                    $('#chat-message').removeClass('invalid');
                    $('#chat-message').val('');
                    $('.chat-messages .list-group').animate({
                        scrollTop: height
                    });
                },
                error: function(data) {
                    $('#chat-message').addClass('invalid');
                    $('#chat-error').removeClass('hidden');
                    $('#chat-error').text(data.responseText);
                }
            });
        }
    });
