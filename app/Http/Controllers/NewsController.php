<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NewsController extends Controller
{
    /**
     * [all description]
     * @return [type] [description]
     */
    public function all() {

    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id) {

    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [edit description]
     * @return [type] [description]
     */
    public function edit() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postEdit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function postEdit(Request $request, $id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [delete description]
     * @return [type] [description]
     */
    public function delete() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }
}
