<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Message;

class MessageController extends Controller
{
    /**
     * [all description]
     * @return [type] [description]
     */
    public function all() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }

        $data = [];
        $data['messages'] = Message::where(['getter_id' => Auth::user()->id])->orWhere(['sender_id' => Auth::user()->id])->get();

        return view('message.all', $data);
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [edit description]
     * @return [type] [description]
     */
    public function edit() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postEdit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function postEdit(Request $request, $id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [delete description]
     * @return [type] [description]
     */
    public function delete() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }
}
