<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use Auth;

use App\User;

/**
 * All the interactions with authenticating user.
 */
class AuthController extends Controller
{
    /**
     * Shows the login form.
     * @return void
     */
    public function getLogin() {
        if(Auth::check()) {
            return redirect()->route('index');
        }

        return view('auth.login');
    }

    /**
     * Checks if the email and password are correct and logs him/her in.
     * @param  Request $request [description]
     * @return void
     */
    public function postLogin(Request $request) {
        if(Auth::check()) {
            return redirect()->route('index');
        }

        $email = $request->input('email');
        $password = $request->input('password');

        if(Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->route('index');
        } else {
            return redirect()->route('auth.login');
        }

    }

    /**
     * Shows the register form.
     * @return void
     */
    public function getRegister() {
        if(Auth::check()) {
            return redirect()->route('index');
        }

        return view('auth.register');
    }

    /**
     * Creates a new account and then logs him/her in and redirects to index.
     * @param  Request $request [description]
     * @return void
     */
    public function postRegister(Request $request) {
        if(Auth::check()) {
            return redirect()->route('index');
        }

        $user = new User;
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->active = 1;
        $user->save();

        Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);

        return redirect()->route('index');
    }

    /**
     * Ends the session and redirects to index.
     * @return void
     */
    public function getLogout() {
        Auth::logout();
        return redirect()->route('auth.login');
    }
}
