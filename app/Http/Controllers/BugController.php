<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Bug;

/**
 * [BugController description]
 */
class BugController extends Controller
{
    /**
     * [all description]
     * @return [type] [description]
     */
    public function all() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }

        $data = [];
        if(Auth::user()->admin > 0) {
            $data['bugs'] = Bug::orderBy('status', 'asc')->paginate(15);
        } else {
            $data['bugs'] = Bug::latest()->paginate(15);
        }

        return view('bugs.all', $data);
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [edit description]
     * @return [type] [description]
     */
    public function edit() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [postEdit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function postEdit(Request $request, $id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }

    /**
     * [delete description]
     * @return [type] [description]
     */
    public function delete() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
    }
}
