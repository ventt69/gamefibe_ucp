<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    /**
     * [all description]
     * @return [type] [description]
     */
    public function all() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }

        $data = [];
        $data['users'] = User::paginate(15);

        return view('user.all', $data);
    }

    public function show($id) {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }

        $data = [];
        $data['user'] = User::findOrFail($id);

        return view('user.show', $data);
    }
}
