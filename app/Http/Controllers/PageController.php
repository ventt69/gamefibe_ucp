<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Shoutbox;

class PageController extends Controller
{
    /**
     * Shows the index page.
     * @return void
     */
    public function index() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }

        return view('pages.index');
    }

    /**
     * Shows the chat page.
     * @return void
     */
    public function chat() {
        if(!Auth::check()) {
            return redirect()->route('auth.login');
        }
        
        $messages = Shoutbox::orderBy('created_at', 'desc')->take(25)->get();

        $data = [];
        $data['shoutboxItems'] = $messages->reverse();

        return view('pages.chat', $data);
    }
}
