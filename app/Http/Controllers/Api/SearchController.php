<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Bug;

class SearchController extends Controller
{
    /**
     * Search the users table.
     * @param  Request $request
     * @return mixed
     */
    public function getUsers(Request $request) {
        $error = ['error' => 'Ühtegi sellise nimega kasutajat ei leitud.'];

        if($request->has('q')) {
            $posts = User::search($request->get('q'))->get();
            return $posts->count() ? $posts : $error;
        }

        return $error;
    }

    /**
     * Search the bugs table.
     * @param  Request $request
     * @return mixed
     */
    public function getBugs(Request $request) {
        $error = ['error' => 'Ühtegi sellise nimega bugi ei leitud.'];

        if($request->has('q')) {
            $posts = Bug::search($request->get('q'))->get();
            return $posts->count() ? $posts : $error;
        }

        return $error;
    }
}
