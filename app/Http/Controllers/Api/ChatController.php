<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Shoutbox;
use App\User;
use Carbon\Carbon;
use \Cache;
use \Auth;
class ChatController extends Controller
{
    public function send(Request $request) {
        if(Auth::user()->active == 0) {
            return 'Su kasutaja pole aktiveeritud.';
        }

        $checkSendRate = Shoutbox::where('user', \Auth::user()->id)->where('created_at', '>=', Carbon::now()->subSeconds(3))->first();
        if($checkSendRate) {
            return 'Palun ära spämmi.';
        }

        $validator = \Validator::make($request->all(), [
            'message' => 'required|max:128',
        ]);
        if ($validator->fails()) {
            return 'Su sõnumiga tekkisid probleemid.';
        }

        if($request->ajax()) {
            preg_match_all('/(@\w+)/', $request->message, $mentions);

            $mentionIDs = [];

            foreach($mentions[0] as $mention) {
                $findUser = User::where('username', 'LIKE', '%' . str_replace('@', '', $mention) . '%')->first();
                if(!empty($findUser->id)) {
                    $mentionIDs[] = $findUser['id'];
                }
            }

            $mentions = implode(',', $mentionIDs);
            if(count($mentions) > 0) {
                $insertMessage = Shoutbox::create(['user' => \Auth::user()->id, 'message' => $request->message, 'mentions' => $mentions]);
            } else {
                $insertMessage = Shoutbox::create(['user' => \Auth::user()->id, 'message' => $request->message]);
            }

            $data = '<li class="list-group-item">
            <div class="profile-avatar tiny pull-left" style="background-image: url(img/avatars/'.e(\Auth::user()->avatar).')"></div>
            <h5 class="list-group-item-heading"><a href="profile/'.e(\Auth::user()->id).'">' . \Auth::user()->username . '</a></h5>
            <p class="message-content"><time>' . $insertMessage->created_at->diffForHumans(). '</time>' . e($request->message) . '</p>
            </li>';
            Cache::forget('shoutbox_messages');
            return \Response::json(['success' => true, 'data' => $data]);
        }
    }


    public function fetch(Request $request) {
        if($request->ajax()) {
            $getData = Cache::remember('shoutbox_messages', 60, function() {
                return Shoutbox::orderBy('created_at', 'desc')->take(25)->get();
            });
            $getData = $getData->reverse();

            $data = [];

            foreach($getData as $messages) {
                $class = '';
                if(in_array(\Auth::user()->id, explode(',', $messages->mentions))) {
                    $class = 'mentioned';
                }
                if(Auth::user()->admin > 0) {
                    $data[] = '<li class="list-group-item '. $class .'">
                    <div class="profile-avatar tiny pull-left" style="background-image: url(img/avatars/'.e($messages->poster->avatar).')"></div>
                    <h5 class="list-group-item-heading"><a href="profile/'.e($messages->poster->id).'">' . e($messages->poster->username) . '</a></h5>
                    <p class="message-content"><time>' . $messages->created_at->diffForHumans() . '</time>' . e($messages->message) . '</p>
                    </li>';
                } else {
                    $data[] = '<li class="list-group-item '. $class .'">
                    <div class="profile-avatar tiny pull-left" style="background-image: url(img/avatars/'.e($messages->poster->avatar).')"></div>
                    <h5 class="list-group-item-heading"><a href="profile/'.e($messages->poster->id).'">' . e($messages->poster->username) . '</a></h5>
                    <p class="message-content"><time>' . $messages->created_at->diffForHumans() . '</time>' . e($messages->message) . '</p>
                    </li>';
                }

            }
            return \Response::json(['success' => true, 'data' => $data]);

        }
    }
}
