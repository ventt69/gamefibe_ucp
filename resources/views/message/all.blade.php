@extends('app')
@section('content')
<br>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Sõnumid</h3>
        </div>
        <div id="messagesearch" class="card-body">
            <message-search></message-search>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Kasutajanimi</th>

                        <th>Pealkiri</th>
                        <th>Loetud</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($messages as $message)
                    @if($message->sender_id == Auth::user()->id)
                    <tr>
                        <td><a href="{{ route('user.show', [$message->getter_id]) }}">{{ DB::table('users')->where(['id' => $message->getter_id])->first()->username }}</a></td>

                        <td><a href="{{ route('message.show', [$message->getter_id]) }}">{{ $message->header }}</a></td>
                        <td>
                            @if($message->seen == 1)
                            <i class="fa fa-btn fa-check"></i>
                            @endif
                         </td>
                    </tr>
                    @else
                    <tr>
                        <td><a href="{{ route('user.show', [$message->sender_id]) }}">{{ DB::table('users')->where(['id' => $message->sender_id])->first()->username }}</a></td>

                        <td><a href="{{ route('message.show', [$message->sender_id]) }}">{{ $message->header }}</a></td>
                        <td>
                            @if($message->seen == 1)
                            <i class="fa fa-btn fa-check"></i>
                            @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
            </div>
        </div>
    </div>
</div>
@stop
