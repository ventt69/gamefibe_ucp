@extends('app')
@section('content')
<br>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Kasutajad</h3>
        </div>
        <div class="card-body">
            <div id="app">
                <users></users>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Kasutajanimi</th>
                        <th>Viimati online</th>
                        <th>Aktiivne</th>
                        <th>Admin</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->username}}</td>
                        <td>{{$user->updated_at}}</td>
                        <td>@if($user->active)<i class="fa fa-check"></i>@endif</td>
                        <td>@if($user->admin > 0)<i class="fa fa-check"></i>@endif</td>
                        <td>
                            <div class="text-center">
                                <a href="{{ route('user.show', [$user->id]) }}" class="btn btn-default btn-xs">Vaata</a>
                                @if(Auth::user()->id != $user->id)
                                    <a href="{{ route('message.create', [$user->id]) }}" class="btn btn-primary btn-sm">Saada sõnum</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $users->links() !!}
            </div>
        </div>
    </div>
</div>
@stop
