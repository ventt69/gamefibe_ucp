@extends('app')
@section('content')
<br>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Bugtracker</h3>
        </div>
        <div class="card-body">
            <div id="app">
                <bugs></bugs>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Bugi nimetus</th><th>Staatus</th><th>Lisaja</th><th>Uuendatud</th>
                    </tr>
                </thead>
                <tbody>
                    @if(\Auth::user()->admin > 0)
                        @foreach($bugs as $bug)
                        <?php
                        if($bug->status == 0) {
                            $bugText = "kontrollimata";
                        } else if($bug->status == 1) {
                            $bugText = "järjekorras";
                        } else if($bug->status == 2) {
                            $bugText = "tagasilükatud";
                        } else if($bug->status == 3) {
                            $bugText = "korras";
                        }
                        ?>
                        <tr>
                            <td><a href="{{ route('bug.show', [$bug->id]) }}">{{ $bug->name }}</a></td>
                            <td>{{ $bugText }}</td>
                            <td><a href="../profile/{{ $bug->bugfinder }}">{{ \DB::table('users')->where(['id' => $bug->bugfinder])->first()->username }}</a></td>
                            <td>{{ \Carbon\Carbon::parse($bug->updated_at)->diffForHumans() }}</td>
                        </tr>
                        @endforeach
                    @elseif(\Auth::user()->admin == 0)
                        @foreach($bugs as $bug)
                        <?php
                        if($bug->status == 0) {
                            $bugText = "kontrollimata";
                        } else if($bug->status == 1) {
                            $bugText = "järjekorras";
                        } else if($bug->status == 2) {
                            $bugText = "tagasilükatud";
                        } else if($bug->status == 3) {
                            $bugText = "korras";
                        }
                        ?>
                        @if(($bug->status == 3) || $bug->bugfinder == \Auth::user()->id)
                            <tr>
                                <td><a href="{{ route('bug.show', [$bug->id]) }}">{{ $bug->name }}</a></td>
                                <td>{{ $bugText }}</td>
                                <td><a href="profile/{{ $bug->bugfinder }}">{{ \DB::table('users')->where(['id' => $bug->bugfinder])->first()->username }}</a></td>
                                <td>{{ \Carbon\Carbon::parse($bug->updated_at)->diffForHumans() }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="text-center">
                {!! $bugs->links() !!}
            </div>
        </div>
    </div>
</div>
@stop
