<nav class="side-navbar">
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar"><img src="../../img/avatars/{{ Auth::user()->avatar }}" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h4">{{ Auth::user()->username }}</h1>
            <p>arendaja</p>
        </div>
    </div>
    <span class="heading">Menüü</span>
    <ul class="list-unstyled">
        <li class="{{ isActiveRoute('index') }}">
            <a href="{{ route('index') }}"><i class="fa fa-home"></i>Avaleht</a>
        </li>
        <li class="{{ isActiveRoute('chat') }}">
            <a href="{{ route('chat') }}"><i class="fa fa-stack-overflow"></i>Shoutbox</a>
        </li>
        <li class="{{ areActiveRoutes(['message.all', 'message.show', 'message.edit', 'message.create']) }}">
            <a href="{{ route('message.all') }}"><i class="fa fa-envelope"></i>Sõnumid</a>
        </li>

        <li class="{{ areActiveRoutes(['bug.all', 'bug.show', 'bug.edit', 'bug.create']) }}">
            <a href="{{ route('bug.all') }}"><i class="fa fa-bug"></i>Bugtracker</a>
        </li>
        <li>
            <a href="#settings" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-wrench"></i>
                Seaded
            </a>
            <ul id="settings" class="collapse list-unstyled">
                <li><a href="#">Kasutaja</a></li>
                <li><a href="#">UCP</a></li>
            </ul>
        </li>
        <li>
            <a href="#server" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-server"></i>
                Server
            </a>
            <ul id="server" class="collapse list-unstyled">
                <li><a href="{{ route('user.all') }}">Kasutajad</a></li>
                <li><a href="#">Töökohad</a></li>
                <li><a href="#">Grupeeringud</a></li>
                <li><a href="#">Töökohad</a></li>
                <li><a href="#">Statistika</a></li>
            </ul>
        </li>
        @if(Auth::user()->admin > 0)
        <li>
            <a href="#admin" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-lock"></i>
                Admin
            </a>
            <ul id="admin" class="collapse list-unstyled">
                <li><a href="#">Logid</a></li>
                <li><a href="#">Server</a></li>
                <li><a href="#">RP testid</a></li>
            </ul>
        </li>
        @endif
    </ul>
</nav>
