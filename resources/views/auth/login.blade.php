<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Gamefibe UCP</title>

        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="favicon.png">
    </head>
    <body>
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">
                        <!-- Logo & Information Panel-->
                        <div class="col-lg-6">
                            <div class="info d-flex align-items-center">
                                <div class="content">
                                    <div class="logo">
                                        <h1>Gamefibe UCP</h1>
                                    </div>
                                    <p>Eesti parim.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">
                                    <form id="login-form" method="post" action="{{ route('auth.login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input id="username" type="email" name="email" required="" class="input-material">
                                            <label for="username" class="label-material">Email</label>
                                        </div>
                                        <div class="form-group">
                                            <input id="password" type="password" name="password" required="" class="input-material">
                                            <label for="password" class="label-material">Parool</label>
                                        </div>
                                        <button type="submit" id="login" class="btn btn-primary">Logi sisse</button>
                                    </form>
                                    <a href="#" class="forgot-pass">Unustasid parooli?</a><br>
                                    <small>Pole kasutajat?</small>
                                    <a href="{{ route('auth.register') }}" class="signup">Registreeru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ elixir('js/app.js') }}" charset="utf-8"></script>
        @yield('script')
    </body>
</html>
