<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Gamefibe UCP</title>

        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="favicon.png">
    </head>
    <body>
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="info d-flex align-items-center">
                                <div class="content">
                                    <div class="logo">
                                        <h1>Gamefibe UCP</h1>
                                    </div>
                                    <p>Eesti parim.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">
                                    <form id="register-form" method="post" action="{{ route('auth.register') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input id="register-username" type="text" name="username" required class="input-material">
                                            <label for="register-username" class="label-material">Kasutajanimi</label>
                                        </div>
                                        <div class="form-group">
                                            <input id="register-email" type="email" name="email" required class="input-material">
                                            <label for="register-email" class="label-material">Emaili aadress</label>
                                        </div>
                                        <div class="form-group">
                                            <input id="register-passowrd" type="password" name="password" required class="input-material">
                                            <label for="register-passowrd" class="label-material">Parool</label>
                                        </div>
                                        <div class="form-group terms-conditions">
                                            <input id="license" type="checkbox" class="checkbox-template">
                                            <label for="license">Nõustun tingimustega</label>
                                        </div>
                                        <input id="register" type="submit" value="Registreeru" class="btn btn-primary">
                                    </form>
                                    <small>Sul juba on kasutaja?</small>
                                    <a href="{{ route('auth.login') }}" class="signup">Logi sisse</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ elixir('js/app.js') }}" charset="utf-8"></script>
        @yield('script')
    </body>
</html>
