@extends('app')

@section('content')
<br>
<div class="col-lg-12">
  <div class="card">
    <div class="card-header d-flex align-items-center">
      <h3 class="h4">Shoutbox</h3>
    </div>
    <div class="card-body">
        <div class="shoutbox">
            <div class="chat-messages">
                <ul class="list-group">
                @foreach($shoutboxItems as $messages)
                <?php
                    $class = '';
                    if(in_array(\Auth::user()->id, explode(',', $messages->mentions))) {
                        $class = 'mentioned';
                    }
                ?>
                <li class="list-group-item {{ $class }}">
                    <div class="profile-avatar tiny pull-left" style="background-image: url(img/avatars/{{ $messages->poster->avatar }})"></div>
                    <h5 class="list-group-item-heading"><a href="profile/{{ $messages->poster->id }}">{{ $messages->poster->username }}</a></h5>
                    <p class="message-content word-wrap"><time>{{ $messages->created_at->diffForHumans() }}</time>{{ $messages->message }}</p>
                </li>
                @endforeach
                </ul>
            </div>
            {!! csrf_field() !!}
            <div class="form-group">
                <input class="form-control" id="chat-message" placeholder="Vajuta enter, et sõnumit saata">
                <p id="chat-error" class="hidden text-danger"></p>
            </div>
        </div>
    </div>
  </div>
</div>


@stop
@section('script')

<script src="{{ URL::asset('js/shoutbox.js') }}" charset="utf-8"></script>
@stop
